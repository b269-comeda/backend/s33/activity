// fetch('https://jsonplaceholder.typicode.com/todos')
// .then((response) => response.json())
// .then((json) => console.log(json));

// arrays of titles
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then(json => {
	let titlesArray = [];
	let titlesOnly = json.map(todo => todo.title);
		titlesArray.push(titlesOnly)
		console.log(titlesArray);
});

// single to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
})
.then((response) => response.json())
.then((json) => console.log(json));

// providing only the status and title of a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
})
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// POST method
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		dateCompleted: "pending",
		description: "To update the my to do list with a different data structure",
		status: "pending",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "30/03/23",
		completed: false,
		status: "Complete",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// DELETE method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',
});